let clientes = [];
// example {id:1592304983049, title: 'Deadpool', year: 2015}
const agregarcliente = (ev)=>{
    ev.preventDefault();  //to stop the form submitting
    let cliente = {
        id: Date.now(),
        nombre: document.getElementById('nombre').value,
        apellido: document.getElementById('apellido').value,
        telefono:document.getElementById('telefono').value
    }
    clientes.push(cliente);
    document.forms[0].reset(); // to clear the form for the next entries
    //document.querySelector('form').reset();

    //for display purposes only
    console.warn('added' , {clientes} );
    let pre = document.querySelector('#message pre');
    pre.textContent = '\n' + JSON.stringify(clientes, '\t', 2);

    //saving to localStorage
    localStorage.setItem('lista', JSON.stringify(clientes) );
}
document.addEventListener('DOMContentLoaded', ()=>{
    document.getElementById('btn').addEventListener('click', agregarcliente);
});