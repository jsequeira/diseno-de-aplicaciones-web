window.onload = function () {
    cargar()
}

function cargar(){
    if(obtenerUsuario()==null){
        document.getElementById('sessionbutton').setAttribute('style','display:none')
        document.getElementById('dashboardbutton').setAttribute('style','display:none')
    }else{
        document.getElementById('registrobutton').setAttribute('style','display:none')
        document.getElementById('loginbutton').setAttribute('style','display:none')
    }

    var articulos = JSON.parse(localStorage.getItem('Articulos'))

    articulos.sort(((a, b) => a.fecha - b.fecha));

    var a="a"
    var img="img"

    for(var numero=0;numero<=1;numero++){

        if(numero==0){
            subirId(articulos[numero].idProducto)
        }
        document.getElementById(a+numero).setAttribute("onclick", "subirId(this.id)")
        document.getElementById(a+numero).setAttribute("id", articulos[numero].idProducto)
        document.getElementById(img+numero).setAttribute("src", articulos[numero].url)
    }

}
function subirId(id){
        
        sessionStorage.setItem('DetalleArticulo', JSON.stringify(id))
}

function cerrarSesion(){
    sessionStorage.removeItem("UsuarioSesion");
    window.location.reload();
}

function obtenerUsuario() {
    var usuario = JSON.parse(sessionStorage.getItem('UsuarioSesion'));
    return usuario
}
