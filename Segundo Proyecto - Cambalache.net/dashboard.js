window.onload = function () {
    cargarArticulos()
}



function cargarArticulos() {
    var nombreUsuario = obtenerUsuario(0).nombre + ' ' + obtenerUsuario(0).apellido;
    document.getElementById('nombre').textContent = nombreUsuario


    var articulos = []

    articulos = JSON.parse(localStorage.getItem('Articulos'))
    if (articulos == null) {
        let lista = document.getElementById('scroll-container')
        var mensaje = document.createElement("h1");
        mensaje.textContent = "No has publicado tus Cambalaches"
        lista.appendChild(mensaje)
        alert('No hay Articulos')
    }

    var boolean = false
    for (var numero = 0; numero < articulos.length; numero++) {

        if (articulos[numero].idUsuario == obtenerUsuario().id) {
            boolean = true
        }
    }
    if (boolean == false) {
        let lista = document.getElementById('scroll-container')
        var mensaje = document.createElement("h1");
        mensaje.textContent = "No has publicado tus Cambalaches"
        lista.appendChild(mensaje)
        alert('No hay Articulos')
    }


    for (var numero = 0; numero < articulos.length; numero++) {

        if (articulos[numero].idUsuario == obtenerUsuario().id) {
            console.log('Articulo encontrado')


            var lista = document.getElementById('scroll-container')

            var parentdiv = document.createElement('div')
            var childdiv = document.createElement('div')
            var a = document.createElement('a')
            var img = document.createElement('img')
            var h2 = document.createElement('h2')
            var br1 = document.createElement('br')
            var br2 = document.createElement('br')
            var btn1 = document.createElement('button')
            var btn2 = document.createElement('button')

            parentdiv.className = "image-container"
            parentdiv.id = articulos[numero].idProducto
            childdiv.className = "asd"
            img.className = "img"
            img.src = articulos[numero].url
            img.alt = ""
            a.appendChild(img)
            h2.textContent = articulos[numero].nombreDeProducto
            btn1.className = "btn1"
            btn1.setAttribute("onclick", 'guardarProductoEditar(this.parentNode.parentNode.id)')
            btn1.textContent = 'Editar'
            btn2.className = "btn2"
            btn2.setAttribute("onclick", 'eliminarArticulo(this.parentNode.parentNode.id)')
            btn2.textContent = 'Eliminar'
            // br1.appendChild(btn1)
            // br2.appendChild(btn2)
            childdiv.appendChild(a)
            childdiv.appendChild(h2)
            childdiv.appendChild(br1)
            childdiv.appendChild(btn1)
            childdiv.appendChild(br2)
            childdiv.appendChild(btn2)
            //childdiv.innerHTML=`<br> <button class="btn1" onclick="location.href='editarproducto.html'">Editar</button><br><button class="btn2" onclick="location.href='dashboard.html'">Eliminar</button>`
            parentdiv.appendChild(childdiv)
            lista.appendChild(parentdiv)
        }

    }

}

function eliminarArticulo(idArticulo) {

    var articulos = JSON.parse(localStorage.getItem('Articulos'))

    for (var numero = 0; numero < articulos.length; numero++) {
        if (articulos[numero].idProducto == idArticulo) {
            alert('Articulo eliminado:' + articulos[numero].idProducto)
            delete articulos[numero]
            var articuloEliminar = document.getElementById(`${idArticulo}`)
            articuloEliminar.parentNode.removeChild(articuloEliminar)

            articulos = articulos.filter(function (dato) {
                return dato != undefined
            });
            guardarArticulos(articulos)
        }
    }

}

function crearArticulo() {

    const lista = document.getElementsByClassName("scroll-container")
    const parentdiv = document.createElement('div')
    const childdiv = document.createElement('div')
    const a = document.createElement('a')
    const img = document.createElement('img')
    const h2 = document.createElement('h2')
    const br = document.createElement('br')
    const br1 = document.createElement('br')
    const btn1 = document.createElement('button')
    const btn2 = document.createElement('button')
    parentdiv.className = "image-container"
    childdiv.className = "asd"
    img.alt = "", img.src = articulos[numero].url
    h2.textContent = articulos[numero].nombredeproducto
    btn1.className = "btn1", btn1.onclick = "location.href='editarproducto.html'"
    btn2.className = "btn2", btn1.onclick = "location.href='editarproducto.html'"
}



function obtenerUsuario() {
    var usuario = JSON.parse(sessionStorage.getItem('UsuarioSesion'));
    return usuario
}

function guardarArticulos(articulos) {
    localStorage.setItem('Articulos', JSON.stringify(articulos))
}

function guardarProductoEditar(idProducto) {

    var articulos = JSON.parse(localStorage.getItem('Articulos'))

    for (var numero = 0; numero < articulos.length; numero++) {
        if (articulos[numero].idProducto == idProducto) {

            var Articulo = {
                idProducto: articulos[numero].idProducto,
                idUsuario: articulos[numero].idUsuario,
                fecha: articulos[numero].fecha,
                nombreCompletoUsuario: articulos[numero].nombreCompletoUsuario,
                nombreDeProducto: articulos[numero].nombreDeProducto,
                descripcion: articulos[numero].descripcion,
                url: articulos[numero].url,
                busco: articulos[numero].busco
            }

            sessionStorage.setItem('Articulo', JSON.stringify(Articulo));
            alert('Articulo a editar')
            window.location = 'editarproducto.html'
        }
    }

}

function cerrarSesion() {
    sessionStorage.removeItem("UsuarioSesion");
    window.location = 'inicio.html'
}