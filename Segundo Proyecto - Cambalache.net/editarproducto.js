window.onload = function () {
    cargarArticulo()
}

function cargarArticulo() {
    var Articulo = obtenerArticuloSession()
    document.getElementById("nombreproducto").value = Articulo.nombreDeProducto
    document.getElementById("descripcion").value = Articulo.descripcion
    document.getElementById("url").value = Articulo.url
    document.getElementById("busco").value = Articulo.busco
}





function guardar() {


    var articulosLocal = obtenerArticulosLocal()
    var articuloSession = obtenerArticuloSession()

    var nuevoArticulo = {

        idProducto: articuloSession.idProducto,
        idUsuario: articuloSession.idUsuario,
        fecha: articuloSession.fecha,
        nombreCompletoUsuario: articuloSession.nombreCompletoUsuario,
        nombreDeProducto: document.getElementById("nombreproducto").value,
        descripcion: document.getElementById("descripcion").value,
        url: document.getElementById("url").value,
        busco: document.getElementById("busco").value


    }

    for (var numero = 0; numero < articulosLocal.length; numero++) {
        if (articulosLocal[numero].idProducto == articuloSession.idProducto) {


            localStorage.removeItem("Articulos");

            delete articulosLocal[numero]

            articulosLocal.push(nuevoArticulo)

            articulosLocal = articulosLocal.filter(function (dato) {
                return dato != undefined
            });

            guardarArticulos(articulosLocal)
            alert('Edición exitosa')
            window.location = 'dashboard.html'
   
            break

        }

    }

}

var form = document.getElementById("myform");
function handleForm(event) { event.preventDefault(); }
form.addEventListener('submit', handleForm);

function obtenerArticulosLocal() {
    var Articulos = JSON.parse(localStorage.getItem('Articulos'));
    return Articulos
}
function obtenerArticuloSession() {
    var Articulo = JSON.parse(sessionStorage.getItem('Articulo'));
    return Articulo
}

function guardarArticulos(articulos) {
    localStorage.setItem('Articulos', JSON.stringify(articulos))
}
